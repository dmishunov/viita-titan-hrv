# VIITA Titan HRV Community-driven Issues List.

This is a community-driven attempt to centralize and monitor all the issues with [VIITA Titan HRV smartwatch](https://design.gitlab.com/components/forms) as there is no such place arranged by VIITA itself. This list is not anyhow managed by VIITA Watches GmbH and is NOT an official communication channel.

## Check existing Issues

https://gitlab.com/mishunov/viita-titan-hrv/issues

## Before Adding An Issue

To avoid duplication, make sure you check [already-known "Issues"](https://gitlab.com/mishunov/viita-titan-hrv/issues) to make sure the issue you're about to report has not yet been reported. If the issue doesn't exist yet, feel free to proceed to the next step.

## To [Add An Issue](https://gitlab.com/mishunov/viita-titan-hrv/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)

You can click the link in this paragraph's heading, OR

* Go to the [list of existing "Issues"](https://gitlab.com/mishunov/viita-titan-hrv/issues).
* Identify the green "New Issue" button at the top right corner.
* Fill out the **details of the issue**. Please be as specific as possible in describing the problem. Images/videos are very helpful for others to know whether this issue is relevant to them or not. You can use [one of the existing issues](https://gitlab.com/mishunov/viita-titan-hrv/issues/1) as an example of filling out the details of the issue.
* If you could classify the reported bug somehow, please add a "label" (separate field in the "New Issue" form). Something like ~"iOS", ~"application", etc. Check [the list of all available labels](https://gitlab.com/mishunov/viita-titan-hrv/-/labels)
* Save

ATTENTION! It is strongly recommended to report the bug or issue you identified directly to VIITA (support@viitawatches.freshdesk.com) and not expect Issues reported here to magically be seen by VIITA team. The Issues here are just for the community to monitor progress on the isues with the watch and the software.